#pragma once
// MESSAGE EMPTY2 PACKING

#define MAVLINK_MSG_ID_EMPTY2 164


typedef struct __mavlink_empty2_t {
 uint64_t null; /*<  NULL*/
} mavlink_empty2_t;

#define MAVLINK_MSG_ID_EMPTY2_LEN 8
#define MAVLINK_MSG_ID_EMPTY2_MIN_LEN 8
#define MAVLINK_MSG_ID_164_LEN 8
#define MAVLINK_MSG_ID_164_MIN_LEN 8

#define MAVLINK_MSG_ID_EMPTY2_CRC 107
#define MAVLINK_MSG_ID_164_CRC 107



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_EMPTY2 { \
    164, \
    "EMPTY2", \
    1, \
    {  { "null", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_empty2_t, null) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_EMPTY2 { \
    "EMPTY2", \
    1, \
    {  { "null", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_empty2_t, null) }, \
         } \
}
#endif

/**
 * @brief Pack a empty2 message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param null  NULL
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_empty2_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint64_t null)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_EMPTY2_LEN];
    _mav_put_uint64_t(buf, 0, null);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_EMPTY2_LEN);
#else
    mavlink_empty2_t packet;
    packet.null = null;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_EMPTY2_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_EMPTY2;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_EMPTY2_MIN_LEN, MAVLINK_MSG_ID_EMPTY2_LEN, MAVLINK_MSG_ID_EMPTY2_CRC);
}

/**
 * @brief Pack a empty2 message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param null  NULL
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_empty2_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint64_t null)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_EMPTY2_LEN];
    _mav_put_uint64_t(buf, 0, null);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_EMPTY2_LEN);
#else
    mavlink_empty2_t packet;
    packet.null = null;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_EMPTY2_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_EMPTY2;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_EMPTY2_MIN_LEN, MAVLINK_MSG_ID_EMPTY2_LEN, MAVLINK_MSG_ID_EMPTY2_CRC);
}

/**
 * @brief Encode a empty2 struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param empty2 C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_empty2_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_empty2_t* empty2)
{
    return mavlink_msg_empty2_pack(system_id, component_id, msg, empty2->null);
}

/**
 * @brief Encode a empty2 struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param empty2 C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_empty2_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_empty2_t* empty2)
{
    return mavlink_msg_empty2_pack_chan(system_id, component_id, chan, msg, empty2->null);
}

/**
 * @brief Send a empty2 message
 * @param chan MAVLink channel to send the message
 *
 * @param null  NULL
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_empty2_send(mavlink_channel_t chan, uint64_t null)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_EMPTY2_LEN];
    _mav_put_uint64_t(buf, 0, null);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_EMPTY2, buf, MAVLINK_MSG_ID_EMPTY2_MIN_LEN, MAVLINK_MSG_ID_EMPTY2_LEN, MAVLINK_MSG_ID_EMPTY2_CRC);
#else
    mavlink_empty2_t packet;
    packet.null = null;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_EMPTY2, (const char *)&packet, MAVLINK_MSG_ID_EMPTY2_MIN_LEN, MAVLINK_MSG_ID_EMPTY2_LEN, MAVLINK_MSG_ID_EMPTY2_CRC);
#endif
}

/**
 * @brief Send a empty2 message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_empty2_send_struct(mavlink_channel_t chan, const mavlink_empty2_t* empty2)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_empty2_send(chan, empty2->null);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_EMPTY2, (const char *)empty2, MAVLINK_MSG_ID_EMPTY2_MIN_LEN, MAVLINK_MSG_ID_EMPTY2_LEN, MAVLINK_MSG_ID_EMPTY2_CRC);
#endif
}

#if MAVLINK_MSG_ID_EMPTY2_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_empty2_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint64_t null)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint64_t(buf, 0, null);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_EMPTY2, buf, MAVLINK_MSG_ID_EMPTY2_MIN_LEN, MAVLINK_MSG_ID_EMPTY2_LEN, MAVLINK_MSG_ID_EMPTY2_CRC);
#else
    mavlink_empty2_t *packet = (mavlink_empty2_t *)msgbuf;
    packet->null = null;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_EMPTY2, (const char *)packet, MAVLINK_MSG_ID_EMPTY2_MIN_LEN, MAVLINK_MSG_ID_EMPTY2_LEN, MAVLINK_MSG_ID_EMPTY2_CRC);
#endif
}
#endif

#endif

// MESSAGE EMPTY2 UNPACKING


/**
 * @brief Get field null from empty2 message
 *
 * @return  NULL
 */
static inline uint64_t mavlink_msg_empty2_get_null(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  0);
}

/**
 * @brief Decode a empty2 message into a struct
 *
 * @param msg The message to decode
 * @param empty2 C-struct to decode the message contents into
 */
static inline void mavlink_msg_empty2_decode(const mavlink_message_t* msg, mavlink_empty2_t* empty2)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    empty2->null = mavlink_msg_empty2_get_null(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_EMPTY2_LEN? msg->len : MAVLINK_MSG_ID_EMPTY2_LEN;
        memset(empty2, 0, MAVLINK_MSG_ID_EMPTY2_LEN);
    memcpy(empty2, _MAV_PAYLOAD(msg), len);
#endif
}
