#pragma once
// MESSAGE SETSTATE PACKING

#define MAVLINK_MSG_ID_SETSTATE 166


typedef struct __mavlink_setstate_t {
 uint64_t timestamp; /*<  time since system start (microseconds)*/
 float x; /*<  North position in NED earth-fixed frame, (metres)*/
 float y; /*<  East position in NED earth-fixed frame, (metres)*/
 float z; /*<  Down position (negative altitude) in NED earth-fixed frame, (metres)*/
 float vx; /*<  North velocity in NED earth-fixed frame, (metres/sec)*/
 float vy; /*<  East velocity in NED earth-fixed frame, (metres/sec)*/
 float vz; /*<  Down velocity in NED earth-fixed frame, (metres/sec)*/
 float airspeed; /*<  Forward airspeed, (metres/sec)*/
 float ax; /*<  North acceleration derivative in NED earth-fixed frame, (metres/sec^2)*/
 float ay; /*<  East acceleration derivative in NED earth-fixed frame, (metres/sec^2)*/
 float az; /*<  Down acceleration derivative in NED earth-fixed frame, (metres/sec^2)*/
 float avx; /*<  North angular velocity in NED earth-fixed frame, (metres/sec)*/
 float avy; /*<  East angular velocity in NED earth-fixed frame, (metres/sec)*/
 float avz; /*<  Down angular velocity in NED earth-fixed frame, (metres/sec)*/
 float aax; /*<  North angular acceleration derivative in NED earth-fixed frame, (metres/sec)*/
 float aay; /*<  East angular acceleration derivative in NED earth-fixed frame, (metres/sec)*/
 float aaz; /*<  Down angular acceleration derivative in NED earth-fixed frame, (metres/sec)*/
 float q[4]; /*<  float array for quaternion*/
 uint8_t start; /*<  Boolean (valid: 1, invalid: 0). Default is 0 (invalid) Start set state (sysid module tells both gazebo and attitude control).*/
 uint8_t heading; /*<  Euler yaw angle transforming the tangent plane relative to NED earth-fixed frame, -PI..+PI,  (radians)*/
 uint8_t TRIM_CONDITION_MODEL; /*<  Set cruise mode by pulling trim parameters*/
} mavlink_setstate_t;

#define MAVLINK_MSG_ID_SETSTATE_LEN 91
#define MAVLINK_MSG_ID_SETSTATE_MIN_LEN 91
#define MAVLINK_MSG_ID_166_LEN 91
#define MAVLINK_MSG_ID_166_MIN_LEN 91

#define MAVLINK_MSG_ID_SETSTATE_CRC 114
#define MAVLINK_MSG_ID_166_CRC 114

#define MAVLINK_MSG_SETSTATE_FIELD_Q_LEN 4

#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_SETSTATE { \
    166, \
    "SETSTATE", \
    21, \
    {  { "timestamp", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_setstate_t, timestamp) }, \
         { "start", NULL, MAVLINK_TYPE_UINT8_T, 0, 88, offsetof(mavlink_setstate_t, start) }, \
         { "x", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_setstate_t, x) }, \
         { "y", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_setstate_t, y) }, \
         { "z", NULL, MAVLINK_TYPE_FLOAT, 0, 16, offsetof(mavlink_setstate_t, z) }, \
         { "vx", NULL, MAVLINK_TYPE_FLOAT, 0, 20, offsetof(mavlink_setstate_t, vx) }, \
         { "vy", NULL, MAVLINK_TYPE_FLOAT, 0, 24, offsetof(mavlink_setstate_t, vy) }, \
         { "vz", NULL, MAVLINK_TYPE_FLOAT, 0, 28, offsetof(mavlink_setstate_t, vz) }, \
         { "airspeed", NULL, MAVLINK_TYPE_FLOAT, 0, 32, offsetof(mavlink_setstate_t, airspeed) }, \
         { "ax", NULL, MAVLINK_TYPE_FLOAT, 0, 36, offsetof(mavlink_setstate_t, ax) }, \
         { "ay", NULL, MAVLINK_TYPE_FLOAT, 0, 40, offsetof(mavlink_setstate_t, ay) }, \
         { "az", NULL, MAVLINK_TYPE_FLOAT, 0, 44, offsetof(mavlink_setstate_t, az) }, \
         { "avx", NULL, MAVLINK_TYPE_FLOAT, 0, 48, offsetof(mavlink_setstate_t, avx) }, \
         { "avy", NULL, MAVLINK_TYPE_FLOAT, 0, 52, offsetof(mavlink_setstate_t, avy) }, \
         { "avz", NULL, MAVLINK_TYPE_FLOAT, 0, 56, offsetof(mavlink_setstate_t, avz) }, \
         { "aax", NULL, MAVLINK_TYPE_FLOAT, 0, 60, offsetof(mavlink_setstate_t, aax) }, \
         { "aay", NULL, MAVLINK_TYPE_FLOAT, 0, 64, offsetof(mavlink_setstate_t, aay) }, \
         { "aaz", NULL, MAVLINK_TYPE_FLOAT, 0, 68, offsetof(mavlink_setstate_t, aaz) }, \
         { "q", NULL, MAVLINK_TYPE_FLOAT, 4, 72, offsetof(mavlink_setstate_t, q) }, \
         { "heading", NULL, MAVLINK_TYPE_UINT8_T, 0, 89, offsetof(mavlink_setstate_t, heading) }, \
         { "TRIM_CONDITION_MODEL", NULL, MAVLINK_TYPE_UINT8_T, 0, 90, offsetof(mavlink_setstate_t, TRIM_CONDITION_MODEL) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_SETSTATE { \
    "SETSTATE", \
    21, \
    {  { "timestamp", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_setstate_t, timestamp) }, \
         { "start", NULL, MAVLINK_TYPE_UINT8_T, 0, 88, offsetof(mavlink_setstate_t, start) }, \
         { "x", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_setstate_t, x) }, \
         { "y", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_setstate_t, y) }, \
         { "z", NULL, MAVLINK_TYPE_FLOAT, 0, 16, offsetof(mavlink_setstate_t, z) }, \
         { "vx", NULL, MAVLINK_TYPE_FLOAT, 0, 20, offsetof(mavlink_setstate_t, vx) }, \
         { "vy", NULL, MAVLINK_TYPE_FLOAT, 0, 24, offsetof(mavlink_setstate_t, vy) }, \
         { "vz", NULL, MAVLINK_TYPE_FLOAT, 0, 28, offsetof(mavlink_setstate_t, vz) }, \
         { "airspeed", NULL, MAVLINK_TYPE_FLOAT, 0, 32, offsetof(mavlink_setstate_t, airspeed) }, \
         { "ax", NULL, MAVLINK_TYPE_FLOAT, 0, 36, offsetof(mavlink_setstate_t, ax) }, \
         { "ay", NULL, MAVLINK_TYPE_FLOAT, 0, 40, offsetof(mavlink_setstate_t, ay) }, \
         { "az", NULL, MAVLINK_TYPE_FLOAT, 0, 44, offsetof(mavlink_setstate_t, az) }, \
         { "avx", NULL, MAVLINK_TYPE_FLOAT, 0, 48, offsetof(mavlink_setstate_t, avx) }, \
         { "avy", NULL, MAVLINK_TYPE_FLOAT, 0, 52, offsetof(mavlink_setstate_t, avy) }, \
         { "avz", NULL, MAVLINK_TYPE_FLOAT, 0, 56, offsetof(mavlink_setstate_t, avz) }, \
         { "aax", NULL, MAVLINK_TYPE_FLOAT, 0, 60, offsetof(mavlink_setstate_t, aax) }, \
         { "aay", NULL, MAVLINK_TYPE_FLOAT, 0, 64, offsetof(mavlink_setstate_t, aay) }, \
         { "aaz", NULL, MAVLINK_TYPE_FLOAT, 0, 68, offsetof(mavlink_setstate_t, aaz) }, \
         { "q", NULL, MAVLINK_TYPE_FLOAT, 4, 72, offsetof(mavlink_setstate_t, q) }, \
         { "heading", NULL, MAVLINK_TYPE_UINT8_T, 0, 89, offsetof(mavlink_setstate_t, heading) }, \
         { "TRIM_CONDITION_MODEL", NULL, MAVLINK_TYPE_UINT8_T, 0, 90, offsetof(mavlink_setstate_t, TRIM_CONDITION_MODEL) }, \
         } \
}
#endif

/**
 * @brief Pack a setstate message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param timestamp  time since system start (microseconds)
 * @param start  Boolean (valid: 1, invalid: 0). Default is 0 (invalid) Start set state (sysid module tells both gazebo and attitude control).
 * @param x  North position in NED earth-fixed frame, (metres)
 * @param y  East position in NED earth-fixed frame, (metres)
 * @param z  Down position (negative altitude) in NED earth-fixed frame, (metres)
 * @param vx  North velocity in NED earth-fixed frame, (metres/sec)
 * @param vy  East velocity in NED earth-fixed frame, (metres/sec)
 * @param vz  Down velocity in NED earth-fixed frame, (metres/sec)
 * @param airspeed  Forward airspeed, (metres/sec)
 * @param ax  North acceleration derivative in NED earth-fixed frame, (metres/sec^2)
 * @param ay  East acceleration derivative in NED earth-fixed frame, (metres/sec^2)
 * @param az  Down acceleration derivative in NED earth-fixed frame, (metres/sec^2)
 * @param avx  North angular velocity in NED earth-fixed frame, (metres/sec)
 * @param avy  East angular velocity in NED earth-fixed frame, (metres/sec)
 * @param avz  Down angular velocity in NED earth-fixed frame, (metres/sec)
 * @param aax  North angular acceleration derivative in NED earth-fixed frame, (metres/sec)
 * @param aay  East angular acceleration derivative in NED earth-fixed frame, (metres/sec)
 * @param aaz  Down angular acceleration derivative in NED earth-fixed frame, (metres/sec)
 * @param q  float array for quaternion
 * @param heading  Euler yaw angle transforming the tangent plane relative to NED earth-fixed frame, -PI..+PI,  (radians)
 * @param TRIM_CONDITION_MODEL  Set cruise mode by pulling trim parameters
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_setstate_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint64_t timestamp, uint8_t start, float x, float y, float z, float vx, float vy, float vz, float airspeed, float ax, float ay, float az, float avx, float avy, float avz, float aax, float aay, float aaz, const float *q, uint8_t heading, uint8_t TRIM_CONDITION_MODEL)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_SETSTATE_LEN];
    _mav_put_uint64_t(buf, 0, timestamp);
    _mav_put_float(buf, 8, x);
    _mav_put_float(buf, 12, y);
    _mav_put_float(buf, 16, z);
    _mav_put_float(buf, 20, vx);
    _mav_put_float(buf, 24, vy);
    _mav_put_float(buf, 28, vz);
    _mav_put_float(buf, 32, airspeed);
    _mav_put_float(buf, 36, ax);
    _mav_put_float(buf, 40, ay);
    _mav_put_float(buf, 44, az);
    _mav_put_float(buf, 48, avx);
    _mav_put_float(buf, 52, avy);
    _mav_put_float(buf, 56, avz);
    _mav_put_float(buf, 60, aax);
    _mav_put_float(buf, 64, aay);
    _mav_put_float(buf, 68, aaz);
    _mav_put_uint8_t(buf, 88, start);
    _mav_put_uint8_t(buf, 89, heading);
    _mav_put_uint8_t(buf, 90, TRIM_CONDITION_MODEL);
    _mav_put_float_array(buf, 72, q, 4);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_SETSTATE_LEN);
#else
    mavlink_setstate_t packet;
    packet.timestamp = timestamp;
    packet.x = x;
    packet.y = y;
    packet.z = z;
    packet.vx = vx;
    packet.vy = vy;
    packet.vz = vz;
    packet.airspeed = airspeed;
    packet.ax = ax;
    packet.ay = ay;
    packet.az = az;
    packet.avx = avx;
    packet.avy = avy;
    packet.avz = avz;
    packet.aax = aax;
    packet.aay = aay;
    packet.aaz = aaz;
    packet.start = start;
    packet.heading = heading;
    packet.TRIM_CONDITION_MODEL = TRIM_CONDITION_MODEL;
    mav_array_memcpy(packet.q, q, sizeof(float)*4);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_SETSTATE_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_SETSTATE;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_SETSTATE_MIN_LEN, MAVLINK_MSG_ID_SETSTATE_LEN, MAVLINK_MSG_ID_SETSTATE_CRC);
}

/**
 * @brief Pack a setstate message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param timestamp  time since system start (microseconds)
 * @param start  Boolean (valid: 1, invalid: 0). Default is 0 (invalid) Start set state (sysid module tells both gazebo and attitude control).
 * @param x  North position in NED earth-fixed frame, (metres)
 * @param y  East position in NED earth-fixed frame, (metres)
 * @param z  Down position (negative altitude) in NED earth-fixed frame, (metres)
 * @param vx  North velocity in NED earth-fixed frame, (metres/sec)
 * @param vy  East velocity in NED earth-fixed frame, (metres/sec)
 * @param vz  Down velocity in NED earth-fixed frame, (metres/sec)
 * @param airspeed  Forward airspeed, (metres/sec)
 * @param ax  North acceleration derivative in NED earth-fixed frame, (metres/sec^2)
 * @param ay  East acceleration derivative in NED earth-fixed frame, (metres/sec^2)
 * @param az  Down acceleration derivative in NED earth-fixed frame, (metres/sec^2)
 * @param avx  North angular velocity in NED earth-fixed frame, (metres/sec)
 * @param avy  East angular velocity in NED earth-fixed frame, (metres/sec)
 * @param avz  Down angular velocity in NED earth-fixed frame, (metres/sec)
 * @param aax  North angular acceleration derivative in NED earth-fixed frame, (metres/sec)
 * @param aay  East angular acceleration derivative in NED earth-fixed frame, (metres/sec)
 * @param aaz  Down angular acceleration derivative in NED earth-fixed frame, (metres/sec)
 * @param q  float array for quaternion
 * @param heading  Euler yaw angle transforming the tangent plane relative to NED earth-fixed frame, -PI..+PI,  (radians)
 * @param TRIM_CONDITION_MODEL  Set cruise mode by pulling trim parameters
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_setstate_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint64_t timestamp,uint8_t start,float x,float y,float z,float vx,float vy,float vz,float airspeed,float ax,float ay,float az,float avx,float avy,float avz,float aax,float aay,float aaz,const float *q,uint8_t heading,uint8_t TRIM_CONDITION_MODEL)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_SETSTATE_LEN];
    _mav_put_uint64_t(buf, 0, timestamp);
    _mav_put_float(buf, 8, x);
    _mav_put_float(buf, 12, y);
    _mav_put_float(buf, 16, z);
    _mav_put_float(buf, 20, vx);
    _mav_put_float(buf, 24, vy);
    _mav_put_float(buf, 28, vz);
    _mav_put_float(buf, 32, airspeed);
    _mav_put_float(buf, 36, ax);
    _mav_put_float(buf, 40, ay);
    _mav_put_float(buf, 44, az);
    _mav_put_float(buf, 48, avx);
    _mav_put_float(buf, 52, avy);
    _mav_put_float(buf, 56, avz);
    _mav_put_float(buf, 60, aax);
    _mav_put_float(buf, 64, aay);
    _mav_put_float(buf, 68, aaz);
    _mav_put_uint8_t(buf, 88, start);
    _mav_put_uint8_t(buf, 89, heading);
    _mav_put_uint8_t(buf, 90, TRIM_CONDITION_MODEL);
    _mav_put_float_array(buf, 72, q, 4);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_SETSTATE_LEN);
#else
    mavlink_setstate_t packet;
    packet.timestamp = timestamp;
    packet.x = x;
    packet.y = y;
    packet.z = z;
    packet.vx = vx;
    packet.vy = vy;
    packet.vz = vz;
    packet.airspeed = airspeed;
    packet.ax = ax;
    packet.ay = ay;
    packet.az = az;
    packet.avx = avx;
    packet.avy = avy;
    packet.avz = avz;
    packet.aax = aax;
    packet.aay = aay;
    packet.aaz = aaz;
    packet.start = start;
    packet.heading = heading;
    packet.TRIM_CONDITION_MODEL = TRIM_CONDITION_MODEL;
    mav_array_memcpy(packet.q, q, sizeof(float)*4);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_SETSTATE_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_SETSTATE;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_SETSTATE_MIN_LEN, MAVLINK_MSG_ID_SETSTATE_LEN, MAVLINK_MSG_ID_SETSTATE_CRC);
}

/**
 * @brief Encode a setstate struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param setstate C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_setstate_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_setstate_t* setstate)
{
    return mavlink_msg_setstate_pack(system_id, component_id, msg, setstate->timestamp, setstate->start, setstate->x, setstate->y, setstate->z, setstate->vx, setstate->vy, setstate->vz, setstate->airspeed, setstate->ax, setstate->ay, setstate->az, setstate->avx, setstate->avy, setstate->avz, setstate->aax, setstate->aay, setstate->aaz, setstate->q, setstate->heading, setstate->TRIM_CONDITION_MODEL);
}

/**
 * @brief Encode a setstate struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param setstate C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_setstate_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_setstate_t* setstate)
{
    return mavlink_msg_setstate_pack_chan(system_id, component_id, chan, msg, setstate->timestamp, setstate->start, setstate->x, setstate->y, setstate->z, setstate->vx, setstate->vy, setstate->vz, setstate->airspeed, setstate->ax, setstate->ay, setstate->az, setstate->avx, setstate->avy, setstate->avz, setstate->aax, setstate->aay, setstate->aaz, setstate->q, setstate->heading, setstate->TRIM_CONDITION_MODEL);
}

/**
 * @brief Send a setstate message
 * @param chan MAVLink channel to send the message
 *
 * @param timestamp  time since system start (microseconds)
 * @param start  Boolean (valid: 1, invalid: 0). Default is 0 (invalid) Start set state (sysid module tells both gazebo and attitude control).
 * @param x  North position in NED earth-fixed frame, (metres)
 * @param y  East position in NED earth-fixed frame, (metres)
 * @param z  Down position (negative altitude) in NED earth-fixed frame, (metres)
 * @param vx  North velocity in NED earth-fixed frame, (metres/sec)
 * @param vy  East velocity in NED earth-fixed frame, (metres/sec)
 * @param vz  Down velocity in NED earth-fixed frame, (metres/sec)
 * @param airspeed  Forward airspeed, (metres/sec)
 * @param ax  North acceleration derivative in NED earth-fixed frame, (metres/sec^2)
 * @param ay  East acceleration derivative in NED earth-fixed frame, (metres/sec^2)
 * @param az  Down acceleration derivative in NED earth-fixed frame, (metres/sec^2)
 * @param avx  North angular velocity in NED earth-fixed frame, (metres/sec)
 * @param avy  East angular velocity in NED earth-fixed frame, (metres/sec)
 * @param avz  Down angular velocity in NED earth-fixed frame, (metres/sec)
 * @param aax  North angular acceleration derivative in NED earth-fixed frame, (metres/sec)
 * @param aay  East angular acceleration derivative in NED earth-fixed frame, (metres/sec)
 * @param aaz  Down angular acceleration derivative in NED earth-fixed frame, (metres/sec)
 * @param q  float array for quaternion
 * @param heading  Euler yaw angle transforming the tangent plane relative to NED earth-fixed frame, -PI..+PI,  (radians)
 * @param TRIM_CONDITION_MODEL  Set cruise mode by pulling trim parameters
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_setstate_send(mavlink_channel_t chan, uint64_t timestamp, uint8_t start, float x, float y, float z, float vx, float vy, float vz, float airspeed, float ax, float ay, float az, float avx, float avy, float avz, float aax, float aay, float aaz, const float *q, uint8_t heading, uint8_t TRIM_CONDITION_MODEL)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_SETSTATE_LEN];
    _mav_put_uint64_t(buf, 0, timestamp);
    _mav_put_float(buf, 8, x);
    _mav_put_float(buf, 12, y);
    _mav_put_float(buf, 16, z);
    _mav_put_float(buf, 20, vx);
    _mav_put_float(buf, 24, vy);
    _mav_put_float(buf, 28, vz);
    _mav_put_float(buf, 32, airspeed);
    _mav_put_float(buf, 36, ax);
    _mav_put_float(buf, 40, ay);
    _mav_put_float(buf, 44, az);
    _mav_put_float(buf, 48, avx);
    _mav_put_float(buf, 52, avy);
    _mav_put_float(buf, 56, avz);
    _mav_put_float(buf, 60, aax);
    _mav_put_float(buf, 64, aay);
    _mav_put_float(buf, 68, aaz);
    _mav_put_uint8_t(buf, 88, start);
    _mav_put_uint8_t(buf, 89, heading);
    _mav_put_uint8_t(buf, 90, TRIM_CONDITION_MODEL);
    _mav_put_float_array(buf, 72, q, 4);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SETSTATE, buf, MAVLINK_MSG_ID_SETSTATE_MIN_LEN, MAVLINK_MSG_ID_SETSTATE_LEN, MAVLINK_MSG_ID_SETSTATE_CRC);
#else
    mavlink_setstate_t packet;
    packet.timestamp = timestamp;
    packet.x = x;
    packet.y = y;
    packet.z = z;
    packet.vx = vx;
    packet.vy = vy;
    packet.vz = vz;
    packet.airspeed = airspeed;
    packet.ax = ax;
    packet.ay = ay;
    packet.az = az;
    packet.avx = avx;
    packet.avy = avy;
    packet.avz = avz;
    packet.aax = aax;
    packet.aay = aay;
    packet.aaz = aaz;
    packet.start = start;
    packet.heading = heading;
    packet.TRIM_CONDITION_MODEL = TRIM_CONDITION_MODEL;
    mav_array_memcpy(packet.q, q, sizeof(float)*4);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SETSTATE, (const char *)&packet, MAVLINK_MSG_ID_SETSTATE_MIN_LEN, MAVLINK_MSG_ID_SETSTATE_LEN, MAVLINK_MSG_ID_SETSTATE_CRC);
#endif
}

/**
 * @brief Send a setstate message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_setstate_send_struct(mavlink_channel_t chan, const mavlink_setstate_t* setstate)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_setstate_send(chan, setstate->timestamp, setstate->start, setstate->x, setstate->y, setstate->z, setstate->vx, setstate->vy, setstate->vz, setstate->airspeed, setstate->ax, setstate->ay, setstate->az, setstate->avx, setstate->avy, setstate->avz, setstate->aax, setstate->aay, setstate->aaz, setstate->q, setstate->heading, setstate->TRIM_CONDITION_MODEL);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SETSTATE, (const char *)setstate, MAVLINK_MSG_ID_SETSTATE_MIN_LEN, MAVLINK_MSG_ID_SETSTATE_LEN, MAVLINK_MSG_ID_SETSTATE_CRC);
#endif
}

#if MAVLINK_MSG_ID_SETSTATE_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_setstate_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint64_t timestamp, uint8_t start, float x, float y, float z, float vx, float vy, float vz, float airspeed, float ax, float ay, float az, float avx, float avy, float avz, float aax, float aay, float aaz, const float *q, uint8_t heading, uint8_t TRIM_CONDITION_MODEL)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint64_t(buf, 0, timestamp);
    _mav_put_float(buf, 8, x);
    _mav_put_float(buf, 12, y);
    _mav_put_float(buf, 16, z);
    _mav_put_float(buf, 20, vx);
    _mav_put_float(buf, 24, vy);
    _mav_put_float(buf, 28, vz);
    _mav_put_float(buf, 32, airspeed);
    _mav_put_float(buf, 36, ax);
    _mav_put_float(buf, 40, ay);
    _mav_put_float(buf, 44, az);
    _mav_put_float(buf, 48, avx);
    _mav_put_float(buf, 52, avy);
    _mav_put_float(buf, 56, avz);
    _mav_put_float(buf, 60, aax);
    _mav_put_float(buf, 64, aay);
    _mav_put_float(buf, 68, aaz);
    _mav_put_uint8_t(buf, 88, start);
    _mav_put_uint8_t(buf, 89, heading);
    _mav_put_uint8_t(buf, 90, TRIM_CONDITION_MODEL);
    _mav_put_float_array(buf, 72, q, 4);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SETSTATE, buf, MAVLINK_MSG_ID_SETSTATE_MIN_LEN, MAVLINK_MSG_ID_SETSTATE_LEN, MAVLINK_MSG_ID_SETSTATE_CRC);
#else
    mavlink_setstate_t *packet = (mavlink_setstate_t *)msgbuf;
    packet->timestamp = timestamp;
    packet->x = x;
    packet->y = y;
    packet->z = z;
    packet->vx = vx;
    packet->vy = vy;
    packet->vz = vz;
    packet->airspeed = airspeed;
    packet->ax = ax;
    packet->ay = ay;
    packet->az = az;
    packet->avx = avx;
    packet->avy = avy;
    packet->avz = avz;
    packet->aax = aax;
    packet->aay = aay;
    packet->aaz = aaz;
    packet->start = start;
    packet->heading = heading;
    packet->TRIM_CONDITION_MODEL = TRIM_CONDITION_MODEL;
    mav_array_memcpy(packet->q, q, sizeof(float)*4);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SETSTATE, (const char *)packet, MAVLINK_MSG_ID_SETSTATE_MIN_LEN, MAVLINK_MSG_ID_SETSTATE_LEN, MAVLINK_MSG_ID_SETSTATE_CRC);
#endif
}
#endif

#endif

// MESSAGE SETSTATE UNPACKING


/**
 * @brief Get field timestamp from setstate message
 *
 * @return  time since system start (microseconds)
 */
static inline uint64_t mavlink_msg_setstate_get_timestamp(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  0);
}

/**
 * @brief Get field start from setstate message
 *
 * @return  Boolean (valid: 1, invalid: 0). Default is 0 (invalid) Start set state (sysid module tells both gazebo and attitude control).
 */
static inline uint8_t mavlink_msg_setstate_get_start(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  88);
}

/**
 * @brief Get field x from setstate message
 *
 * @return  North position in NED earth-fixed frame, (metres)
 */
static inline float mavlink_msg_setstate_get_x(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  8);
}

/**
 * @brief Get field y from setstate message
 *
 * @return  East position in NED earth-fixed frame, (metres)
 */
static inline float mavlink_msg_setstate_get_y(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  12);
}

/**
 * @brief Get field z from setstate message
 *
 * @return  Down position (negative altitude) in NED earth-fixed frame, (metres)
 */
static inline float mavlink_msg_setstate_get_z(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  16);
}

/**
 * @brief Get field vx from setstate message
 *
 * @return  North velocity in NED earth-fixed frame, (metres/sec)
 */
static inline float mavlink_msg_setstate_get_vx(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  20);
}

/**
 * @brief Get field vy from setstate message
 *
 * @return  East velocity in NED earth-fixed frame, (metres/sec)
 */
static inline float mavlink_msg_setstate_get_vy(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  24);
}

/**
 * @brief Get field vz from setstate message
 *
 * @return  Down velocity in NED earth-fixed frame, (metres/sec)
 */
static inline float mavlink_msg_setstate_get_vz(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  28);
}

/**
 * @brief Get field airspeed from setstate message
 *
 * @return  Forward airspeed, (metres/sec)
 */
static inline float mavlink_msg_setstate_get_airspeed(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  32);
}

/**
 * @brief Get field ax from setstate message
 *
 * @return  North acceleration derivative in NED earth-fixed frame, (metres/sec^2)
 */
static inline float mavlink_msg_setstate_get_ax(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  36);
}

/**
 * @brief Get field ay from setstate message
 *
 * @return  East acceleration derivative in NED earth-fixed frame, (metres/sec^2)
 */
static inline float mavlink_msg_setstate_get_ay(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  40);
}

/**
 * @brief Get field az from setstate message
 *
 * @return  Down acceleration derivative in NED earth-fixed frame, (metres/sec^2)
 */
static inline float mavlink_msg_setstate_get_az(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  44);
}

/**
 * @brief Get field avx from setstate message
 *
 * @return  North angular velocity in NED earth-fixed frame, (metres/sec)
 */
static inline float mavlink_msg_setstate_get_avx(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  48);
}

/**
 * @brief Get field avy from setstate message
 *
 * @return  East angular velocity in NED earth-fixed frame, (metres/sec)
 */
static inline float mavlink_msg_setstate_get_avy(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  52);
}

/**
 * @brief Get field avz from setstate message
 *
 * @return  Down angular velocity in NED earth-fixed frame, (metres/sec)
 */
static inline float mavlink_msg_setstate_get_avz(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  56);
}

/**
 * @brief Get field aax from setstate message
 *
 * @return  North angular acceleration derivative in NED earth-fixed frame, (metres/sec)
 */
static inline float mavlink_msg_setstate_get_aax(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  60);
}

/**
 * @brief Get field aay from setstate message
 *
 * @return  East angular acceleration derivative in NED earth-fixed frame, (metres/sec)
 */
static inline float mavlink_msg_setstate_get_aay(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  64);
}

/**
 * @brief Get field aaz from setstate message
 *
 * @return  Down angular acceleration derivative in NED earth-fixed frame, (metres/sec)
 */
static inline float mavlink_msg_setstate_get_aaz(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  68);
}

/**
 * @brief Get field q from setstate message
 *
 * @return  float array for quaternion
 */
static inline uint16_t mavlink_msg_setstate_get_q(const mavlink_message_t* msg, float *q)
{
    return _MAV_RETURN_float_array(msg, q, 4,  72);
}

/**
 * @brief Get field heading from setstate message
 *
 * @return  Euler yaw angle transforming the tangent plane relative to NED earth-fixed frame, -PI..+PI,  (radians)
 */
static inline uint8_t mavlink_msg_setstate_get_heading(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  89);
}

/**
 * @brief Get field TRIM_CONDITION_MODEL from setstate message
 *
 * @return  Set cruise mode by pulling trim parameters
 */
static inline uint8_t mavlink_msg_setstate_get_TRIM_CONDITION_MODEL(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  90);
}

/**
 * @brief Decode a setstate message into a struct
 *
 * @param msg The message to decode
 * @param setstate C-struct to decode the message contents into
 */
static inline void mavlink_msg_setstate_decode(const mavlink_message_t* msg, mavlink_setstate_t* setstate)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    setstate->timestamp = mavlink_msg_setstate_get_timestamp(msg);
    setstate->x = mavlink_msg_setstate_get_x(msg);
    setstate->y = mavlink_msg_setstate_get_y(msg);
    setstate->z = mavlink_msg_setstate_get_z(msg);
    setstate->vx = mavlink_msg_setstate_get_vx(msg);
    setstate->vy = mavlink_msg_setstate_get_vy(msg);
    setstate->vz = mavlink_msg_setstate_get_vz(msg);
    setstate->airspeed = mavlink_msg_setstate_get_airspeed(msg);
    setstate->ax = mavlink_msg_setstate_get_ax(msg);
    setstate->ay = mavlink_msg_setstate_get_ay(msg);
    setstate->az = mavlink_msg_setstate_get_az(msg);
    setstate->avx = mavlink_msg_setstate_get_avx(msg);
    setstate->avy = mavlink_msg_setstate_get_avy(msg);
    setstate->avz = mavlink_msg_setstate_get_avz(msg);
    setstate->aax = mavlink_msg_setstate_get_aax(msg);
    setstate->aay = mavlink_msg_setstate_get_aay(msg);
    setstate->aaz = mavlink_msg_setstate_get_aaz(msg);
    mavlink_msg_setstate_get_q(msg, setstate->q);
    setstate->start = mavlink_msg_setstate_get_start(msg);
    setstate->heading = mavlink_msg_setstate_get_heading(msg);
    setstate->TRIM_CONDITION_MODEL = mavlink_msg_setstate_get_TRIM_CONDITION_MODEL(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_SETSTATE_LEN? msg->len : MAVLINK_MSG_ID_SETSTATE_LEN;
        memset(setstate, 0, MAVLINK_MSG_ID_SETSTATE_LEN);
    memcpy(setstate, _MAV_PAYLOAD(msg), len);
#endif
}
