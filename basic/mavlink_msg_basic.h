#pragma once
// MESSAGE BASIC PACKING

#define MAVLINK_MSG_ID_BASIC 167


typedef struct __mavlink_basic_t {
 uint64_t timestamp; /*<  time since system start (microseconds)*/
} mavlink_basic_t;

#define MAVLINK_MSG_ID_BASIC_LEN 8
#define MAVLINK_MSG_ID_BASIC_MIN_LEN 8
#define MAVLINK_MSG_ID_167_LEN 8
#define MAVLINK_MSG_ID_167_MIN_LEN 8

#define MAVLINK_MSG_ID_BASIC_CRC 140
#define MAVLINK_MSG_ID_167_CRC 140



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_BASIC { \
    167, \
    "BASIC", \
    1, \
    {  { "timestamp", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_basic_t, timestamp) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_BASIC { \
    "BASIC", \
    1, \
    {  { "timestamp", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_basic_t, timestamp) }, \
         } \
}
#endif

/**
 * @brief Pack a basic message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param timestamp  time since system start (microseconds)
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_basic_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint64_t timestamp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_BASIC_LEN];
    _mav_put_uint64_t(buf, 0, timestamp);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_BASIC_LEN);
#else
    mavlink_basic_t packet;
    packet.timestamp = timestamp;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_BASIC_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_BASIC;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_BASIC_MIN_LEN, MAVLINK_MSG_ID_BASIC_LEN, MAVLINK_MSG_ID_BASIC_CRC);
}

/**
 * @brief Pack a basic message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param timestamp  time since system start (microseconds)
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_basic_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint64_t timestamp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_BASIC_LEN];
    _mav_put_uint64_t(buf, 0, timestamp);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_BASIC_LEN);
#else
    mavlink_basic_t packet;
    packet.timestamp = timestamp;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_BASIC_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_BASIC;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_BASIC_MIN_LEN, MAVLINK_MSG_ID_BASIC_LEN, MAVLINK_MSG_ID_BASIC_CRC);
}

/**
 * @brief Encode a basic struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param basic C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_basic_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_basic_t* basic)
{
    return mavlink_msg_basic_pack(system_id, component_id, msg, basic->timestamp);
}

/**
 * @brief Encode a basic struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param basic C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_basic_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_basic_t* basic)
{
    return mavlink_msg_basic_pack_chan(system_id, component_id, chan, msg, basic->timestamp);
}

/**
 * @brief Send a basic message
 * @param chan MAVLink channel to send the message
 *
 * @param timestamp  time since system start (microseconds)
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_basic_send(mavlink_channel_t chan, uint64_t timestamp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_BASIC_LEN];
    _mav_put_uint64_t(buf, 0, timestamp);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_BASIC, buf, MAVLINK_MSG_ID_BASIC_MIN_LEN, MAVLINK_MSG_ID_BASIC_LEN, MAVLINK_MSG_ID_BASIC_CRC);
#else
    mavlink_basic_t packet;
    packet.timestamp = timestamp;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_BASIC, (const char *)&packet, MAVLINK_MSG_ID_BASIC_MIN_LEN, MAVLINK_MSG_ID_BASIC_LEN, MAVLINK_MSG_ID_BASIC_CRC);
#endif
}

/**
 * @brief Send a basic message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_basic_send_struct(mavlink_channel_t chan, const mavlink_basic_t* basic)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_basic_send(chan, basic->timestamp);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_BASIC, (const char *)basic, MAVLINK_MSG_ID_BASIC_MIN_LEN, MAVLINK_MSG_ID_BASIC_LEN, MAVLINK_MSG_ID_BASIC_CRC);
#endif
}

#if MAVLINK_MSG_ID_BASIC_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_basic_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint64_t timestamp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint64_t(buf, 0, timestamp);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_BASIC, buf, MAVLINK_MSG_ID_BASIC_MIN_LEN, MAVLINK_MSG_ID_BASIC_LEN, MAVLINK_MSG_ID_BASIC_CRC);
#else
    mavlink_basic_t *packet = (mavlink_basic_t *)msgbuf;
    packet->timestamp = timestamp;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_BASIC, (const char *)packet, MAVLINK_MSG_ID_BASIC_MIN_LEN, MAVLINK_MSG_ID_BASIC_LEN, MAVLINK_MSG_ID_BASIC_CRC);
#endif
}
#endif

#endif

// MESSAGE BASIC UNPACKING


/**
 * @brief Get field timestamp from basic message
 *
 * @return  time since system start (microseconds)
 */
static inline uint64_t mavlink_msg_basic_get_timestamp(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  0);
}

/**
 * @brief Decode a basic message into a struct
 *
 * @param msg The message to decode
 * @param basic C-struct to decode the message contents into
 */
static inline void mavlink_msg_basic_decode(const mavlink_message_t* msg, mavlink_basic_t* basic)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    basic->timestamp = mavlink_msg_basic_get_timestamp(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_BASIC_LEN? msg->len : MAVLINK_MSG_ID_BASIC_LEN;
        memset(basic, 0, MAVLINK_MSG_ID_BASIC_LEN);
    memcpy(basic, _MAV_PAYLOAD(msg), len);
#endif
}
